var config = {};
try {
    config = require("./config.json");
} catch (error) {
    console.error("have to create config.json with tgToken field");
    progress.exit(1);
}

if (!config.tgToken) {
    console.error("config.json need to have tgToken field");
    progress.exit(2);
}
const request = require("request");

const screenSize = config.screenSize || 12;


const screenSpace = (new Array(screenSize + 1)).toString().replace(/,/g, ' '); // ' ' * screenSize in python
const baseApiUrl = 'https://api.telegram.org/bot' + config.tgToken;

function callTgApi(method, payload) {
    return new Promise(function (resolve, reject) {
        request.post(baseApiUrl + '/' + method, {
            json: true,
            body: payload || {}
        }, function (error, response, body) {
            if (error) {
                return reject(error);
            }
            if (!body.ok) {
                return reject(body);
            }
            resolve(body.result);
        });
    });
}

var _offset = 0;

function receiverLoop() {
    callTgApi('getUpdates', {
        timeout: 60,
        offset: _offset
    }).then(function (updates) {
        updates.map(checkUpdate);
        process.nextTick(receiverLoop);
    }, function (err) {
        console.trace(err);
    });
}

function checkUpdate(update) {
    if (_offset <= update["update_id"]) {
        _offset = update["update_id"] + 1;
    }
    if (update.message && update.message.text === '/start') {
        createCalculator(update.message.chat.id);
        return;
    }
    if (update.callback_query) {
        const callbackQuery = update.callback_query;
        const messageId = callbackQuery.message.message_id;
        const key = callbackQuery.message.chat.id + ":" + messageId;
        if (!activeCalculators[key]) {
            activeCalculators[key] = new Calculator(callbackQuery.message);
        }
        activeCalculators[key].buttonPressed(callbackQuery.data, callbackQuery.id);
    }
}

function createMessage(chatId, regX, operator, regY) {
    const screen = [];
    if (regY !== null && operator && regY !== undefined) {
        screen.push((screenSpace + regY.toString()).substr(-screenSize));
        screen.push(operator)
    }
    screen.push((screenSpace + regX.toString()).substr(-screenSize));

    return {
        chat_id: chatId,
        text: '<pre>' + screen.join('\n') + '</pre>',
        parse_mode: "HTML",
        reply_markup: {
            inline_keyboard: [
                createButtons("AC + -".split(" ")),
                createButtons("1 2 3".split(" ")),
                createButtons("4 5 6".split(" ")),
                createButtons("7 8 9".split(" ")),
                createButtons("  ,0,=".split(","))
            ]
        }
    };
}

function createButtons(buttons) {
    return buttons.map(function (button) {
        return {text: button, callback_data: button};
    });
}

function createCalculator(chatId) {
    var message = createMessage(chatId, 0);

    callTgApi('sendMessage', message).then(
        function (sentMessage) {
        }, function (err) {
            console.trace(err);
        }
    );
}


function Calculator(message) {
    const chatId = message.chat.id;
    const messageId = message.message_id;
    var regX = 0, regY = 0, isResult = false, operator = null, lastIsOperator = false;
    var screen = message.text.split('\n');
    if (screen.length === 3) {
        regY = parseInt(screen[0].trim(), 10);
        operator = screen[1].trim();
        regX = parseInt(screen[2].trim(), 10);
        if (operator !== '+' && operator !== '-') {
            regY = null;
            operator = null;
        }
    } else {
        regX = parseInt(message.text.trim(), 10);
    }

    console.log("Start calculator with :", regY, operator, regX);
    // public
    this.buttonPressed = function (button, callbackQueryId) {
        if (actButton(button.trim())) {
            updateMsg();
        }
        confirmButton(callbackQueryId);
    };

    // private
    function actButton(button) {
        if (button >= "0" && button <= "9") {
            if (lastIsOperator || isResult) {
                regX = 0;
                isResult = false;
            }
            lastIsOperator = false;
            if (regX.toString(10).length >= screenSize) {
                return false;
            }
            regX = regX * 10 + 1 * button;
            return true;
        }
        if (button === 'AC') {
            regX = 0;
            regY = 0;
            operator = null;
            return true;
        }
        if (button === '+' || button === '-') {
            if (!isResult && operator) {
                calc();
            }
            regY = regX;
            operator = button;
            lastIsOperator = true;
            return true;
        }
        if (button === '=' && operator && !isResult) {
            return calc();
        }
        return false;
    }

    function calc() {
        isResult = true;
        if (regY !== null) {
            switch (operator) {
                case '+':
                    regX = regY + regX;
                    break;
                case '-':
                    regX = regY - regX;
                    break;
            }
            regY = null;
            operator = null;
            return true
        }
        return false;
    }

    function confirmButton(callbackQueryId) {
        callTgApi("answerCallbackQuery", {
            callback_query_id: callbackQueryId
        }).catch(function (error) {
            console.error('In answerCallbackQuery call - ', error);
        });
    }

    function updateMsg() {
        const message =
            createMessage(chatId, regX, lastIsOperator ? null : operator, regY);
        message.message_id = messageId;
        callTgApi("editMessageText", message).then(function (res) {

        }, function (err) {
            console.error('error update', err);
        });
    }
}

const activeCalculators = {};

function main() {
    receiverLoop();
}

main();